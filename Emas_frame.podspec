Pod::Spec.new do |s|

  s.name         = "Emas_frame"
  s.version      = "1.0.0"
  s.summary      = "Emas_frame类库"

  s.description  = <<-DESC
                   * Detail about BioService framework.
                   DESC

  s.homepage     = "http://"
  s.license      = 'MIT (example)'
  s.author       = { "未定义" => "undefined" }
  s.platform     = :ios, '8.0'
  s.ios.deployment_target = '8.0'
  s.source       = { :http => "http://Emas_frame.zip" }
  s.preserve_paths = "BioService.framework/*"
  s.resources  = "Emas_frame.framework/*.{bundle,xcassets}"
  s.vendored_frameworks = 'Emas_frame.framework'
  s.requires_arc = true
  s.xcconfig = { 'FRAMEWORK_SEARCH_PATHS' => '$(PODS_ROOT)/Emas_frame' }

  #s.dependency 'XXXX'

end
