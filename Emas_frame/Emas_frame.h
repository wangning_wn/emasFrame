//
//  Emas_frame.h
//  Emas_frame
//
//  Created by 汪宁 on 2018/5/4.
//  Copyright © 2018年 WN. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for Emas_frame.
FOUNDATION_EXPORT double Emas_frameVersionNumber;

//! Project version string for Emas_frame.
FOUNDATION_EXPORT const unsigned char Emas_frameVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <Emas_frame/PublicHeader.h>

#import<Emas_frame/Service.h>
